# Einführung

## Wie ist das Buch aufgebaut?

Das Buch orientiert sich am Prozess der Datenanalyse, der am Ende dieses Kapitels vorgestellt wird. Daher finden Sie in diesem Buch zu jedem Schritt ein eigenes Kapitel, in dem die wichtigsten Themen besprochen werden. Bevor die Umsetzung der einzelnen Schritte in R erklärt wird, werden im nächsten Kapitel die Grundlagen von R vermittelt.

Die praktische Durchführung einer Datenanalyse steht im Mittelpunkt. Daher werden im Buch die wichtigsten Aspekte betrachtet, die man in den meisten Fällen benötigt. Dies bedeutet aber auch, dass nicht jedes Thema in seiner kompletten Tiefe behandelt werden kann. Im Buch werden Sie daher auch Hinweise zu weiterführender Literatur finden. Auch ist es immer empfehlenswert, bei einem speziellen Problem oder einer speziellen Herausforderung in R auf der Plattform stackoverflow nachzuschauen. Auf dieser Plattform kann man Fragen und Probleme schildern und erhält Antworten. Bevor man eine eigene Frage oder Problem veröffentlicht, sollte man aber nach dem Problem auf der Plattform suchen. In den meisten Fällen gibt es schon entsprechende Antworten.

Im Buch wird mit Beispieldatensätzen gearbeitet. Die meisten Datensätze werden mit R-Paketen veröffentlicht oder sind bereits in R enthalten. Daher ist es nicht nötig, die Datensätze zuerst aus dem Internet herunterzuladen. Sollte dies doch mal der Fall sein, sehen Sie einen entsprechenden Hinweis an der jeweiligen Stelle.

Da die praktische Anwendung bei diesem Buch im Vordergrund steht, werden zu allen behandelten Themen die entsprechenden Funktionen und Syntax besprochen. Hierbei werden die Erläuterungen zur Syntax immer unter dem jeweiligen Code-Block aufgeführt. Wenn Sie auf die jeweilige Nummer der Erläuterung klicken, wird in der HTML-Version des Buches die entsprechende Zeile im Code-Block markiert. Weiterhin haben Sie in der HTML-Version auch die Möglichkeit, den Code-Block zu kopieren und in Ihr R-Skript oder Ihre R-Konsole zu übernehmen. Hier ein Beispiel:

``` r
library(dplyr) # <1>
library(palmerpenguins) # <2>

penguins |> # <3>
  glimpse() # <4>
```

1.  Zuerst wird das Paket `dplyr` geladen, das verschiedene Funktionen zum Datenmanagement enthält.
2.  Danach wird das Paket `palmerpenguins` geladen. Dieses enthält den Datensatz `penguins`.
3.  Der Datensatz `penguins` wird ausgewählt und der Pipe-Operator (`|>`) wird eingefügt. Diesen kann man lesen als "wird weitergeleitet zu".
4.  Die Funktion `glimpse()` aus dem dplyr-Paket wird aufgerufen. Die Funktion gibt einen Überblick über den Datensatz als Output zurück.

Es wird folgende Übersicht über den Datensatz erzeugt:

```{r}
#| echo: false
#| warning: false

library(dplyr) 
library(palmerpenguins) 

penguins |> 
  glimpse() 
```

Der Output wird im Kapitel "Daten bereinigen" genauer erläutert.

Da die praktische Durchführung einer Datenanalyse im Mittelpunkt des Buches steht, wird auch - soweit möglich - auf statistische Formeln verzichtet. Die einzelnen Verfahren werden allgemein verständlich beschrieben, damit diese nachvollzogen werden können. Wer sich für die statistischen Hintergründe interessiert, dem sei das Buch von Eid, Gollwitzer und Schmitt [-@eidStatistikUndForschungsmethoden2017] empfohlen.

## Für wen ist das Buch?

Dieses Buch richtet sich sowohl an Einsteiger als auch an Personen, die bereits Erfahrung mit der Datenanalyse in R haben. Für Einsteiger empfiehlt es sich, das Buch klassisch von vorn nach hinten zu lesen. Nach der Lektüre sollten die wichtigsten Konzepte und Funktionen in R bekannt sein, um eine eigene Datenanalyse in R durchzuführen.

Leserinnen und Leser, die bereits Erfahrung mit R haben, können sich gezielt die Kapitel aussuchen, zu denen Sie Fragen haben oder in die Sie sich tiefer einarbeiten möchten.

## Prozess der Datenanalyse

Möchte man Daten analysieren, sind mehrere Schritte nötig. In @fig-prozess-daten werden die einzelnen Schritte des Prozesses der Datenanalyse dargestellt [in Anlehnung an @wickhamDataScienceImport2017, p. ix]:

```{mermaid}
%%| label: fig-prozess-daten
%%| fig-cap: Prozess der Datenanalyse.

flowchart TD
  A[Daten importieren]
  B[Daten bereinigen]
  C[Datenmanagement]
  D[Visualisierung]
  E[Analyse]
  F[Berichten]
  A --> B
  B --> C
  C --> D
  D --> E
  E --> C
  E --> F
```

Im Prozess der Datenanalyse wird davon ausgegangen, dass die Daten, die analysiert werden sollen, bereits erhoben wurden. Daher beginnt dieser mit dem **Import von Daten bzw. einem Datensatz**. Sollten die Daten bisher nicht vorliegen und noch erhoben werden müssen, sei an dieser Stelle auf die Bücher von König [-@konigPraxisforschungSozialenArbeit2016] bzw. Döring [-@doeringForschungsmethodenUndEvaluation2023] verwiesen, in denen auch erläutert wird, wie Daten erhoben werden können.

Wurden die Daten importiert, ist oft der nächste Schritt im Prozess der Datenanalyse die **Datenbereinigung**. Hierbei wird als Erstes geprüft, ob die Daten richtig eingelesen wurden. Bei der Prüfung wird untersucht, ob die Variablen den richtigen Variablentyp haben, ob die Wertebereiche passen oder bei der Erfassung der Daten evtl. falsche Werte erfasst wurden. Auch wird in diesem Schritt geprüft, ob in den einzelnen Variablen fehlende Werte vorliegen, und es kann eine Entscheidung getroffen werden, wie mit diesen umgegangen werden soll. Am Ende dieses Schrittes sollte der Datensatz so vorliegen, dass er gut für die Analyse geeignet ist. Im R-Kosmos spricht man auch von einem 'tidy' Datensatz.

Der nächste Schritt ist dann das **Datenmanagement**. Hierunter werden alle Vorgänge gefasst, um die Daten für die Analyse auszuwählen. Dies kann die Auswahl von bestimmten Variablen, aber auch das Filtern nach bestimmten Fällen sein. Dieser Schritt wird auch im Rahmen der Datenanalyse mehrmals durchlaufen, beispielsweise wenn neue Variablen gebildet oder berechnet werden müssen, die für die Analyse nötig sind.

Hat man die Daten für die Analyse vorbereitet, macht es Sinn, in einem ersten Schritt einen Eindruck von den Daten zu bekommen. Hierbei werden vor allem Techniken der **Visualisierung** eingesetzt, aber auch Methoden der deskriptiven Statistik und der explorativen Datenanalyse. In Lehrbüchern findet man beide Begriffe, in diesem Buch werden diese synonym verwendet, da beide Analysearten das Ziel haben, die Daten besser kennenzulernen und zu beschreiben.

Im eigentlichen Schritt der **Analyse** können verschiedene Analysemethoden eingesetzt werden. So können Zusammenhänge näher untersucht, Unterschiede zwischen Gruppen oder Veränderungen über die Zeit überprüft werden. Auch sind mit multivariaten Methoden die Analyse komplexer Fragestellungen und Prognosen möglich.  Im Rahmen der Datenanalyse empfiehlt sich ein hypothesengeleitetes Vorgehen. Darunter versteht man, dass vor der Analyse schon eine Vorstellung über Hypothesen und Fragestellungen vorhanden ist. Dies bedeutet aber nicht, dass man im Prozess nicht auch weitere Fragestellungen mitaufnehmen kann. Daher sind in @fig-prozess-daten die Schritte Datenmanagement, Visualisierung und Analyse als ein Kreislauf dargestellt.

Wurde die Datenanalyse abgeschlossen, ist der letzte Schritt das **Berichten** der Ergebnisse. Hierzu können verschiedene Formen gewählt werden. So können die Ergebnisse präsentiert und/oder in einem Bericht aufbereitet werden. Neben diesen Formen stehen auch interaktive Möglichkeiten zur Verfügung, wie beispielsweise die Zurverfügungstellung über Dashboards oder sogenannte API-Schnittstellen.

In den nachfolgenden Kapiteln dieses Buches wird aufgezeigt, wie die einzelnen Schritte in R durchlaufen werden können.
