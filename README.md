# Buch Praktische Datenanalyse mit R

Repository des Online-Buches "Praktische Datenanalyse mit R". Das Buch wird aktuell geschrieben. Daher wird das Repository laufend erweitert.

Das Online-Buch kann unter [https://r-buch.sebastian-ottmann.de](https://r-buch.sebastian-ottmann.de) abgerufen werden.

# Book Praktische Datenanalyse mit R

Repository of the online book "Praktische Datenanalyse mit R". The book is currently being written. The repository is therefore constantly being expanded. The book is published in German.

The online book can be accessed at [https://r-buch.sebastian-ottmann.de](https://r-buch.sebastian-ottmann.de).